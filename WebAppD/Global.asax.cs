﻿using System.Collections.Generic;
using System.Web;
using System.Web.Http;
using System.Web.Optimization;
using System.Web.Routing;
using WebAppD.Models;

namespace WebAppD
{
    public class WebApiApplication : HttpApplication
    {
        public static Dictionary<int, Buss> ApplicationWideBussRepository;
        public static Dictionary<int, Driver> ApplicationWideDriverRepository;

        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            ApplicationWideBussRepository = new Dictionary<int, Buss>()
            {
                {
                    0,
                    new Buss
                    {
                        Id = 0,
                        Brand = "Scania",
                        Capacity = 63,
                        DriverIds = new[] {9, 4}
                    }
                },
                {
                    1,
                    new Buss
                    {
                        Brand = "Mulag",
                        Capacity = 100,
                        DriverIds = new[] {9},
                        Id = 1
                    }
                }
            };
            ApplicationWideDriverRepository = new Dictionary<int, Driver>();

            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}