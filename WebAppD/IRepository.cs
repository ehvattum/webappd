﻿using System.Collections.Generic;
using WebAppD.Models;

namespace WebAppD
{
    public interface IRepository
    {
        Buss Put(int id, Buss buss);
        Buss[] Busses();
        Buss Create(Buss buss);
        void Delete(int id);
        Driver[] Drivers();
        List<Driver> GetDriversOnTheBus(int bussId);
        Driver CreateDriver(Driver driver);
        Buss GetBuss(int id);
    }
}