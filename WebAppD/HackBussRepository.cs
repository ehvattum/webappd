using System;
using System.Collections.Generic;
using System.Linq;
using WebAppD.Models;

namespace WebAppD
{
    public class HackBussRepository : IRepository
    {
        public Buss[] Busses()
        {
            var buss = new[]
            {
                new Buss
                {
                    Id = 6,
                    Brand = "Scania",
                    Capacity = 63,
                    DriverIds = new[] {9, 4}
                },
                new Buss
                {
                    Brand = "Mulag",
                    Capacity = 100,
                    DriverIds = new[] {9},
                    Id = 1337
                }
            };
            return buss;
        }

        public Buss GetBuss(int id)
        {
            throw new NotImplementedException();
        }

        public Buss Put(int id, Buss buss)
        {
            throw new NotImplementedException();
        }

        public Driver CreateDriver(Driver driver)
        {
            throw new NotImplementedException();
        }

        public Driver[] Drivers()
        {
            var buss = new[]
            {
                new Driver
                {
                    Id = 4,
                    Name = "Dick Cheney",
                    BirthDate = DateTime.Parse("1930.02.12"),
                },
                new Driver
                {
                    Name = "Arne Garborg",
                    BirthDate = DateTime.Parse("1999.09.12"),
                    Id = 9
                }
            };
            return buss;
        }

        public List<Driver> GetDriversOnTheBus(int bussId)
        {
            Buss[] busses = Busses();

            Buss buss = null;
            foreach (Buss bus in busses)
            {
                if (bus.Id == bussId)
                {
                    buss = bus;
                }
            }
            if (buss == null)
                throw new ApplicationException("Missing buss driver");

            var drivers = Drivers();

            List<Driver> driversOnTheBus = new List<Driver>();

            foreach (Driver driver in drivers)
            {
                if (buss.DriverIds.Contains(driver.Id))
                {
                    driversOnTheBus.Add(driver);
                }
            }
            return driversOnTheBus;
        }

        public Buss Create(Buss buss)
        {
            buss.Id = 13;
            return buss;
        }

        public void Delete(int id)
        {
            
        }
    }
}