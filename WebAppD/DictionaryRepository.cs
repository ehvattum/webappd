﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAppD.Models;


namespace WebAppD
{
    public class DictionaryRepository : IRepository
    {

        public Driver CreateDriver(Driver driver)
        {
            int idOnNewDriver = this.driverStore.Count();
            driver.Id = idOnNewDriver;
            driverStore.Add(idOnNewDriver, driver);
            return driver;
        }

        public Buss Put(int id, Buss buss)
        {
            buss.Id = id;
            bussStore[id] = buss;
            return buss;
        }

        public DictionaryRepository()
        {
            this.bussStore = WebApiApplication.ApplicationWideBussRepository;
            this.driverStore = WebApiApplication.ApplicationWideDriverRepository;
        }

        private readonly Dictionary<int, Buss> bussStore;
        private readonly Dictionary<int, Driver> driverStore;
        

        public Buss[] Busses()
        {
            return bussStore.Values.ToArray();
        }

        public Driver[] Drivers()
        {
            Driver[] allDrivers = driverStore.Values.ToArray();
            return allDrivers;
        }

        public Buss GetBuss(int id)
        {
            Buss[] busses = Busses();

            Buss buss = null;
            foreach (Buss bus in busses)
            {
                if (bus.Id == id)
                {
                    buss = bus;
                }
            }
            return buss;

        }

        public List<Driver> GetDriversOnTheBus(int bussId)
        {
            Buss buss = GetBuss(bussId);
            
            if (buss == null)
                throw new ApplicationException("Missing buss driver");

            int[] driverIds = buss.DriverIds;
            var drivers = Drivers();

            List<Driver> driversOnTheBus = new List<Driver>();

            foreach (Driver driver in drivers)
            {
                if (driverIds.Contains(driver.Id))
                {
                    driversOnTheBus.Add(driver);
                }
            }
            return driversOnTheBus;
        }

        public Buss Create(Buss buss)
        {
            int idOnNewBuss = bussStore.Count();
            while (bussStore.ContainsKey(idOnNewBuss))
            {
                idOnNewBuss++;
            }
            buss.Id = idOnNewBuss;
            bussStore.Add(idOnNewBuss, buss);
            return buss;
        }

        public void Delete(int id)
        {
            bussStore.Remove(id);
        }
    }
}