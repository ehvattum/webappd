﻿using System.ComponentModel.DataAnnotations;

namespace WebAppD.Models
{
    public class Buss
    {
        [Required]
        [StringLength(600)]
        public string Brand { get; set; }
        public int Capacity { get; set; }
        public int[] DriverIds { get; set; }
        public int Id { get; set; }
    }
}