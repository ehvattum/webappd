﻿using System;

namespace WebAppD.Models
{
    public class Driver
    {
        public DateTime BirthDate { get; set; }

        public int Id { get; set; }
        public string Name { get; set; }
    }
}