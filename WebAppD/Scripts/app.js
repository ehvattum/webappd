﻿$(function() {

    var printBuss = function(element, buss) {
        element.append("<tr>"
            + '<td>'
            + buss.Id
            + '</td>'
            + '<td>'
            + buss.Brand
            + '</td>'
            + '<td>'
            + buss.Capacity
            + '</td>'
            + '<td>'
            + ' <button type="button" name="delete" data-id="' + buss.Id + '" class="btn btn-danger">delete</button>'
            + '</td>'
            + '</tr>'
        );
    };

    $('#busstable tbody:first').on('click', '.btn-danger', function () {
        $.ajax({
                url: 'api/buss/' + $(this).attr('data-id'),
                type: 'DELETE'
        })
            .done(function() {
                getBusses();
            });
    });
    var getBusses = function() {
        $.ajax({
            url: 'api/buss'
        }).done(function(data) {
            var table = $('#busstable tbody:first');
            table.html('');
            for (var i = 0; i < data.length; i++) {
                printBuss(table, data[i]);
            }
        });
    };

    $('#addBuss').on('click', function() {
        var buss = {
            Brand: $('#brand').val(),
            Capacity: $('#capacity').val(),
            DriverList: $('#drivers').val().split(',').map(Number)
        };
        $.ajax({
            url: 'api/buss',
            accept: 'application/json',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(buss)
        }).done(function(data) {
            printBuss($('#busstable tbody:first'), data);
        });

    });

    $('#editBuss').on('click', function() {
        var buss = {
            Id: $('#editId').val(),
            Brand: $('#editBrand').val(),
            Capacity: $('#editCapacity').val(),
            DriverList: $('#editDrivers').val().split(',').map(Number)
        };
        $.ajax({
            url: 'api/buss/' + buss.Id,
            accept: 'application/json',
            type: 'PUT',
            contentType: 'application/json',
            data: JSON.stringify(buss)
        }).done(function() {

            getBusses();
        });
    });
    getBusses();
});