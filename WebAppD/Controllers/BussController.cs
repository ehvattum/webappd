﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAppD.Models;

namespace WebAppD.Controllers
{
    public class BussController : ApiController
    {
        private readonly IRepository bussRepository;

        public BussController()
        {
            bussRepository = new DictionaryRepository();
        }

        [Route("api/buss/{id}")]
        public HttpResponseMessage Delete(int id)
        {
            Buss buss = bussRepository.GetBuss(id);
            if (buss == null)
                return Request.CreateResponse(HttpStatusCode.NotFound);
            else
                bussRepository.Delete(id);

                return Request.CreateResponse(HttpStatusCode.OK);
        }

        [Route("api/buss/{id}")]
        public HttpResponseMessage Put(int id, [FromBody]Buss buss)
        {
            if (buss == null)
                return Request.CreateResponse(HttpStatusCode.BadRequest);

            Buss existing = bussRepository.GetBuss(id);
            HttpStatusCode returnCode;
            if (existing == null)
                returnCode = HttpStatusCode.Created;
            else
                  returnCode = HttpStatusCode.OK;
            
            Buss toReturn = bussRepository.Put(id, buss);
            return Request.CreateResponse(returnCode, toReturn);

        }


        [Route("api/buss/")]
        [HttpPost]
        public HttpResponseMessage Post(Buss buss)
        {
            this.Validate(buss);
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest,ModelState);

            Buss value = bussRepository.Create(buss);
            return Request.CreateResponse(HttpStatusCode.Created, value);
        }

        [Route("api/buss/")]
        public HttpResponseMessage Get()
        {
            var busses = bussRepository.Busses();
            return Request.CreateResponse(busses);
        }

        [Route("api/buss/{id}")]
        public HttpResponseMessage Get(int id)
        {
            Buss buss = bussRepository.GetBuss(id);
            
            if (buss != null)
                return Request.CreateResponse(buss);
            else
                return Request.CreateResponse(HttpStatusCode.NotFound);
        }

        // http://localhost:{someport}/api/buss/3/drivers/8
        [Route("api/buss/{bussId}/drivers")]
        public HttpResponseMessage GetDriversOnThisBus(int bussId)
        {
            try
            {
                List<Driver> driversOnTheBus = bussRepository.GetDriversOnTheBus(bussId);
                HttpResponseMessage returnValue = Request.CreateResponse(driversOnTheBus);
                return returnValue;
            }
            catch (ApplicationException e)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            catch (Exception e)
            {
                //log me
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
            
            
        }
    }
}