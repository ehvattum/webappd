﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAppD.Models;

namespace WebAppD.Controllers
{
    public class DriversController : ApiController
    {
        public DriversController ()
        {
            this.repository = new DictionaryRepository();
            
        }

        private readonly IRepository repository;

        [Route("api/drivers")]
        [HttpGet]
        public HttpResponseMessage Get()
        {
            var drivers = repository.Drivers();
            return Request.CreateResponse(drivers);
        }

        [HttpPost]
        [Route("api/drivers")]
        public HttpResponseMessage Post(Driver driver)
        {
            if (driver == null)
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            driver = repository.CreateDriver(driver);
            return Request.CreateResponse(HttpStatusCode.Created, driver);

        }

    }
}